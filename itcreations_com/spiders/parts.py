# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.spiders import SitemapSpider


class PartsSpider(SitemapSpider):
    name = 'parts'
    allowed_domains = ['itcreations.com']
    sitemap_urls = ['https://www.itcreations.com/sitemap.xml']
    sitemap_rules = [('view_product\.asp', 'parse')]


    def parse(self, response):
        d = {}

        d['url'] = response.url

        d['title'] = response.css('div.h1 p[itemprop="name"] ::text').extract_first()
        if d['title']:
            d['title'] = d['title'].strip()

        d['part_number'] = response.css('div[itemprop="offers"]>b ::text').extract_first()
        if d['part_number']:
            d['part_number'] = d['part_number'].strip()

        d['availability'] = response.css('span[itemprop="availability"] ::text').extract_first()
        if d['availability']:
            d['availability'] = d['availability'].strip()

        d['qty'] = response.css('span.count ::text').extract_first()
        if d['qty']:
            d['qty'] = d['qty'].strip().lstrip('0')

        d['price'] = response.css('span[itemprop="price"] ::text').extract_first()
        if d['price']:
            d['price'] = d['price'].strip()

        d['image_urls'] = response.css('div.bodyContainer a.lightwindow ::attr(href)').extract()

        d['details'] = self.get_details(response)

        return d


    def get_details(self, response):
        d = {}

        pattern1 ='<B><FONT.*?>\s*([^<>]+?)\s*</B></FONT>\s*([^<>]+?)\s*<BR>'
        pattern2 = '<B><FONT.*?>\s*([^<>]+?)\s*</B></FONT><B><FONT.*?>\s*([^<>]+?)\s*<'

        for t in re.findall(pattern1, response.text):
            d[t[0].rstrip(':')] = t[1]

        for t in re.findall(pattern2, response.text):
            d[t[0].rstrip(':')] = t[1]

        # print(d)

        return d